import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.math.BigInteger;
import java.util.Set;

class sample{
    public static void main(String args[]){

        Graph<Integer,Integer> g = new Graph<Integer,Integer>();
        g.setDirected(true);
    //    g.setDirected(false);   // for MST directed graph does not mean anything
        g.addEdge(0,1,11);
        g.addEdge(0,2,12);
        g.addEdge(1,3,12);
        g.addEdge(2,1,1);
        g.addEdge(2,4,11);
        g.addEdge(3,5,19);
        g.addEdge(4,5,4);
        g.addEdge(4,3,7);
    //    g.addEdge(3,2,4);

        GraphAlgorithms<Integer,Integer> galgo = new GraphAlgorithms<Integer,Integer>();

        ArrayList<Integer> path ;
        // Breadth first search
        path = galgo.bfs(g,0,3);
       // printArrayList(path);

        // Depth first search
        path = galgo.dfs(g,0,3);
      //  printArrayList(path);

        // Dijkstra's shortest path
        path = galgo.dijkstras(g,0,3);
      //  printArrayList(path);

        Graph<Integer,Double> allPaths = galgo.floydWarshall(g);
        for(Integer i : allPaths.getNodes()){
            for(Integer j : allPaths.getAdjSet(i)){
                try{
                    System.out.println(i+" -> "+j+" = "+allPaths.getEdgeCost(i,j));
                }catch(Exception e){}
            }
        }

    //    // Prim's Minimum Spanning Tree
    //    System.out.println("************** Prim's MST *****************");
        Graph<Integer,Integer> mst = galgo.primsMST(g);
    //    printUndirectedGraph(mst);

    //    System.out.println("************* Kruskal MST *****************");
        Graph<Integer,Integer> mst2 = galgo.kruskalMST(g);
    //    printUndirectedGraph(mst2);

        // Topological Sort
        try{
            path = galgo.topologicalSort(g);
   //         printArrayList(path);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }

        //Belman-Ford parent map
        HashMap<Integer,Integer> map = galgo.bellmanFord(g,0);
//        printMap(map);

        //Print all paths
        Set<ArrayList<Integer>> set = galgo.getAllPaths(g,0,5);
//        printPathMap(set);
    }

    static void printArrayList(ArrayList<Integer> path){
        for(int i = 0; i<path.size(); i++){
            System.out.println(path.get(i));
        }
    }

    static void printGraph(Graph<Integer,Integer> mst){
        Set<Integer> nodes = mst.getNodes();
        Set<Integer> nodes2 = mst.getNodes();
        for(Integer i : nodes){
            for(Integer j : nodes2){
                if(i != j){
                    try{
                        int cost = mst.getEdgeCost(i,j);
                        System.out.println(i+" ---"+cost+"--> "+j);
                    }catch(Exception e){

                    }
                }
            }
        }
    }

    static void printUndirectedGraph(Graph<Integer,Integer> mst){
        Set<Integer> nodes = mst.getNodes();
        Set<Integer> nodes2 = mst.getNodes();
        boolean [][] done = new boolean[nodes.size()][nodes.size()];
        for(Integer i : nodes){
            for(Integer j : nodes2){
                if(i != j){
                    if(done[i][j])  continue;
                    try{
                        int cost = mst.getEdgeCost(i,j);
                        done[i][j] = true;
                        done[j][i] = true;
                        System.out.println(i+" <--"+cost+"--> "+j);
                    }catch(Exception e){

                    }
                }
            }
        }
    }

    static void printMap(HashMap<Integer,Integer> map){
        for(Map.Entry e : map.entrySet() ){
            System.out.println(e.getKey() + " - > " + e.getValue() );
        }
    }

    static void printPathMap(Set<ArrayList<Integer>> set){
        for(ArrayList<Integer> n : set){
            for(Integer i : n){
                System.out.print(i+" -> ");
            }
            System.out.println();
        }
    }
}
