import java.util.ArrayList;
import java.util.Set;
import java.util.Queue;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.TreeSet;

public class GraphAlgorithms<node,cost> {

    /**
     * bfs - Breadth First Search Algorithm
     * @param graph the Graph object on which BFS is searched
     * @param source start node for the search
     * @param sink end node for the search
     * @return list of nodes from source to list
     */
    public ArrayList<node> bfs(Graph<node,cost> graph,node source, node sink){

        LinkedList<node> adjList = new LinkedList<node>();
        adjList.push(source);

        HashMap<node,node> parents = new HashMap<node,node>();
        parents.put(source,null );

        while(! adjList.isEmpty() ){
            node s = adjList.poll();

            if(sink.equals(s)){
                //finish searching
                ArrayList<node> path = new ArrayList<node>();
                node curr = s;
                while(curr != null){
                    path.add(curr);
                    curr = parents.get(curr);
                }
                Collections.reverse(path);
                return path;
            }else{
                Set<node> childrenOfS = graph.getAdjSet(s);
                for(node n : childrenOfS ){
                    if(parents.containsKey(n)){
                        //this node is already found ignore it
                    }else{
                        parents.put(n,s);
                        adjList.addLast(n);
                    }
                }
            }
        }
        return new ArrayList<node>();
    }

    /**
     * dfs - Depth first search algorithm
     * @param graph Graph object on which DFS is performed
     * @param source Source node of the search
     * @param sink Sink node of the search
     * @return a list of nodes from source to sink
     */
    public ArrayList<node> dfs(Graph<node,cost> graph,node source, node sink){

        LinkedList<node> adjList = new LinkedList<node>();
        adjList.push(source);

        HashMap<node,node> parents = new HashMap<node,node>();
        parents.put(source,null );

        while(! adjList.isEmpty() ){
            node s = adjList.poll();

            if(sink.equals(s)){
                //finish searching
                ArrayList<node> path = new ArrayList<node>();
                node curr = s;
                while(curr != null){
                    path.add(curr);
                    curr = parents.get(curr);
                }
                Collections.reverse(path);
                return path;
            }else{
                Set<node> childrenOfS = graph.getAdjSet(s);
                for(node n : childrenOfS ){
                    if(parents.containsKey(n)){
                        //this node is already found ignore it
                    }else{
                        parents.put(n,s);
                        adjList.push(n);
                    }
                }
            }
        }
        return new ArrayList<node>();
    }

    /**
     * getAllPaths get all paths from a source to sink
     * uses the dfs method to search for a path from source to sink
     * @param graph Graph object on which searching is done
     * @param source starting node of the search
     * @param sink end node of the search
     * @return Set of ArrayList<node> where each ArrayList is a path from source to sink
     */
     Set<ArrayList<node>> getAllPaths(Graph<node,cost> graph, node source, node sink){
        //use a simple class to store the parent
        class NodeClass{
            node name;
            NodeClass parent;

            NodeClass(node n,NodeClass p){
                this.name = n;
                this.parent = p;
            }
        }

        LinkedList<NodeClass> adjList = new LinkedList<NodeClass>();
        adjList.push(new NodeClass(source,null));

        Set<ArrayList<node>> pathSet = new HashSet<ArrayList<node>>();

        while(! adjList.isEmpty() ){
            NodeClass nClass = adjList.poll();
            node s = nClass.name;

            if(sink.equals(s)){ // Are you avoiding the cycles ? YES
                //finish searching
                ArrayList<node> path = new ArrayList<node>();
                NodeClass currClass = nClass;
                while(currClass != null){
                    path.add(currClass.name);
                    currClass = currClass.parent;
                }
                Collections.reverse(path);

                pathSet.add(path);
            }else{
                Set<node> childrenOfS = graph.getAdjSet(s);
                for(node n : childrenOfS ){
                        NodeClass nxtNode = new NodeClass(n,nClass);
                        NodeClass tmp = nClass;
                        while(tmp != null && tmp.name != nxtNode.name ){
                            tmp = tmp.parent;
                        }
                        if(tmp == null){    // add the child only if there is no cycles
                            adjList.push(nxtNode);
                        }
                }
            }
        }
        return pathSet;
     }

    /**
     * dijkstras - Find shortes path from source to sink
     * This function will work for Byte, Double, Float, Integer, Long, Short. <br>
     * Will have to reimplement for BigInteger and other classes. <br>
     * 10000 is considered to be inifinity if it is not may have to change the source. <br>
     * dijkstras will not give a correct result if edges contain negative weights. There is no checks for this in this method implementation <br>
     * @param graph Graph object on wich search is performed
     * @param source Start node for the search
     * @param sink end node for the search
     * @return ArrayList which contains the nodes from source to sink
     */
    public ArrayList<node> dijkstras(Graph<node,cost> graph,node source,node sink){
        //Simple class to store the parent and distance
        class NodeClass implements Comparable<NodeClass> {
            node name;
            node parent;
            double distance;    //using the double because it can contain int,float,double
            short color;    // 1- WHITE 2-GRAY 3-BLACk

            @Override
            public int compareTo(NodeClass nc){
                double diff = this.distance - nc.distance;
                if(diff > 0) return 1;
                else if(diff < 0) return -1;
                else return 0;
            }
        }

        HashMap<node,NodeClass> names = new HashMap<node,NodeClass>();

        Set<node> nodes = graph.getNodes();

        for(node n : nodes){
            NodeClass nc = new NodeClass();
            nc.name = n;
            nc.parent = null;
            nc.color = (short) 1; //Mark as WHITE (Undiscovered)
            nc.distance = 10000; // 10000 is used instead of Inifinity
            names.put(n, nc);
        }
        NodeClass ncSource = names.get(source);
        if(ncSource == null) return null; //dijkstra's only be performed if the source exsists in graph
        ncSource.distance = 0;

        PriorityQueue<NodeClass> pq = new PriorityQueue<NodeClass>();
        pq.add(ncSource);

        while(pq.size() > 0){
            NodeClass curr = pq.poll();
            curr.color = (short) 3; //Mark as BLACK (Visited)

            if(curr.name == sink){ //search complete... return the path
                ArrayList<node> path = new ArrayList<node>();
                NodeClass tmp = curr;
                while(tmp.parent != null){
                    path.add(tmp.name);
                    tmp = names.get(tmp.parent);
                }
                path.add(tmp.name);
                Collections.reverse(path);
                return path;
            }

            //curr is not equal to sink continue searching...
            Set<node> adjSet = graph.getAdjSet(curr.name);
            for(node n : adjSet){
                NodeClass next = names.get(n);
                if(next != null && next.color != (short) 3 /*Not Black*/){
                    try{
                        double newDistance = curr.distance + ((Number)graph.getEdgeCost(curr.name, next.name)).doubleValue();
                        if(next.distance > newDistance){
							if(next.color == (short) 2 /*GRAY*/){
								pq.remove(next);	// We'll up update the weight and put it back
								next.color = (short) 1;
							}
                            next.distance = newDistance;
                            next.parent = curr.name;
                        }
                        if(next.color == (short) 1 /*WHITE*/){
                            pq.add(next);
                            next.color = (short) 2; //Mark as GRAY (Discovered)
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                        System.err.println("Result may not be correct! ");
                    }
                }
            }
        }
        //path is not found
        return null;
    }

    /**
     * Bellman-Ford algorithm finds shortes path from a source to every other node
     * This should be used when the graph contains negative weighted edges
     * The source can be edited to get the cost of the each node
     * When getting the cost by this map return will be type HashMap(node,Double)()
     * @param graph Object on which bellman-Ford performed
     * @param source Source node to algorithm
     * @return HashMap containing the parent of each node
     */
     public HashMap<node,node> bellmanFord(Graph<node,cost> graph, node source){
         //Simple class to store the parent and distance
        class NodeClass {
            node name;
            node parent;
            double distance;    //using the double because it can contain int,float,double
        }

        HashMap<node,NodeClass> names = new HashMap<node,NodeClass>();

        Set<node> nodes = graph.getNodes();

        for(node n : nodes){
            NodeClass nc = new NodeClass();
            nc.name = n;
            nc.parent = null;
            nc.distance = 10000; // 10000 is used instead of Inifinity
            names.put(n, nc);
        }

        NodeClass ncSource = names.get(source);
        ncSource.distance = 0;

        for(int i = 0 ; i < nodes.size()-1 ; i++ ){
             for( node n : nodes ){
                 Set<node> adj = graph.getAdjSet(n);
                 double parentDis = names.get(n).distance;
                 for( node m : adj ){
                     double childDis = names.get(m).distance;
                     try{
                         double d = ((Number) graph.getEdgeCost(n,m)).doubleValue();
                         if( childDis > parentDis + d ){
                             NodeClass child = names.get(m);
                             child.distance = parentDis + d;
                             child.parent = n;
                         }
                     }catch(Exception e){
                         // will not come here
                     }
                 }
             }
        }
        // Following line returns parent map

        HashMap<node,node> parentMap = new HashMap<node,node>();
        for(node n : nodes){
            parentMap.put(n, names.get(n).parent );
        }
        return parentMap;

        // Following line returns distance map
        /*
        HashMap<node,cost> costMap = new HashMap<node,cost>();
        for(node n : nodes){
            costMap.put(n, (cost) Double.valueOf(names.get(n).distance) );
        }
        return costMap;
        */
     }

    /**
     * Floyd Warshall - All paths shortest path<br>
     * gives a distance between each node as a double valueOf
     * @param graph - The Graph object where shortest paths calculated
     * @return A Graph object which has a edge between two nodes if there is a path between them. edge weight is the shortest distance.
     */
     public Graph<node,Double> floydWarshall(Graph<node,cost> graph){
         Set<node> nodes = graph.getNodes();
         int n = nodes.size();
         Map<Integer,node> map = new HashMap<Integer,node>();
         double[][] mat = new double[n][n];
         double INIFINITY = 1000000;
         int index = 0;
         for(node nn : nodes){
             map.put(index,nn);
             mat[index][index] = 0;
             index++;
         }
         for(int i=0;i<n;i++){
             for(int j=0;j<n;j++){
                 if(i==j) continue;
                 try{
                     mat[i][j] = ((Number)graph.getEdgeCost(map.get(i),map.get(j))).doubleValue();
                 }catch(Exception e){
                     mat[i][j] = INIFINITY;
                 }
             }
         }

         for(int k=0;k<n;k++){
             for(int i=0;i<n;i++){
                 for(int j=0;j<n;j++){
                     if(mat[i][j] > mat[i][k]+mat[k][j]){
                         mat[i][j] = mat[i][k]+mat[k][j];
                     }
                 }
             }
         }
         Graph<node,Double> disMat = new Graph<node,Double>();
         disMat.setDirected(true);
         for(int i=0;i<n;i++){
             for(int j=0;j<n;j++){
                 double d = mat[i][j];
                 if(mat[i][j]==INIFINITY) continue;
                 disMat.addEdge(map.get(i),map.get(j),mat[i][j]);
             }
         }

         return disMat;
     }

    /**
     * Prim's - Minimum spanning tree algorithm<br>
     * Wikipedia says it is performed on an undirected graph<br>
	 * If the input graph is undirected returned MST graph is also undirected
     * @param graph Graph object on which prim's performed
     * @return A graph containing a subset of edges such that it can create a minimum spanning tree
     */
     public Graph<node,cost> primsMST(Graph<node,cost> graph){
         //Simple class to store the parent and distance
        class NodeClass implements Comparable<NodeClass> {
            node name;
            node parent;
            double distance;    //using the double because it can contain int,float,double
            short color;    // 1- WHITE 2-GRAY 3-BLACk

            @Override
            public int compareTo(NodeClass nc){
                double diff = this.distance - nc.distance;
                if(diff > 0) return 1;
                else if(diff < 0) return -1;
                else return 0;
            }
        }

        HashMap<node,NodeClass> names = new HashMap<node,NodeClass>();

        Set<node> nodes = graph.getNodes();

        for(node n : nodes){
            NodeClass nc = new NodeClass();
            nc.name = n;
            nc.parent = null;
            nc.color = (short) 1; //Mark as WHITE (Undiscovered)
            nc.distance = 10000; // 10000 is used instead of Inifinity
            names.put(n, nc);
        }

        Set<node> nodeSet = graph.getNodes();
        if(nodeSet.isEmpty()) return null;      //no nodes in the graph

        node root = nodeSet.iterator().next();

        NodeClass ncRoot = names.get(root);
        if(ncRoot == null) return null; //prime's only be performed if the root exsists in graph
        ncRoot.distance = 0;

        PriorityQueue<NodeClass> pq = new PriorityQueue<NodeClass>();
        pq.add(ncRoot);

        while(pq.size() > 0){
            NodeClass curr = pq.poll();
            curr.color = (short) 3; //Mark as BLACK (Visited)

            //curr is not equal to sink continue searching...
            Set<node> adjSet = graph.getAdjSet(curr.name);
            for(node n : adjSet){
                NodeClass next = names.get(n);
                if(next != null && next.color != (short) 3 /*Not Black*/){
                    try{
                        double newDistance = ((Number)graph.getEdgeCost(curr.name, next.name)).doubleValue();
                        if(next.distance > newDistance){
							if(next.color == (short) 2 /*GRAY*/){	//Already in pq
								pq.remove(next);
								next.color = (short) 1;
							}
                            next.distance = newDistance;
                            next.parent = curr.name;
                        }
                        if(next.color == (short) 1 /*WHITE*/){
                            pq.add(next);
                            next.color = (short) 2; //Mark as GRAY (Discovered)
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                        System.err.println("Result may not be correct! ");
                    }
                }
            }
        }
        //Generate the new graph with the subset of edges
        Graph<node,cost> mst = new Graph<node,cost>();
		mst.setDirected(graph.isDirected());
        for(Map.Entry<node,NodeClass> entry : names.entrySet() ){
            NodeClass nc = entry.getValue();
            if(nc.parent != null){
                try{
                    mst.addEdge(nc.parent, nc.name, graph.getEdgeCost(nc.parent, nc.name) );
                }catch(Exception e){
                    //hopefully we dont come here
                }
            }
        }
        return mst;
     }

     /**
     * KruskalMST serves the same purpose served by Prim's algorithm<br>
     * This algorithm need disjointSet which is implemented using TreeSet and HashMap here.
     * @param graph Graph object on which minimum spanning tree is searched.
     * @return Graph object which represent a Minimum spanning tree.
     */
     public Graph<node,cost> kruskalMST(Graph<node,cost> graph){
         class EdgeClass implements Comparable<EdgeClass> {
             node from;
             node to;
             cost weight;    //using the double because it can contain int,float,double

             public EdgeClass(node a,node b,cost c){
                 this.from = a;
                 this.to   = b;
                 this.weight = c;
             }
             @Override
             public int compareTo(EdgeClass ec){
                 double diff = ((Number)this.weight).doubleValue() - ((Number)ec.weight).doubleValue();
                 if(diff > 0) return 1;
                 else if(diff < 0) return -1;
                 else return 0;
             }
         }
         PriorityQueue<EdgeClass> pq = new PriorityQueue<EdgeClass>();
         Map<node,TreeSet<node>> disjointSet = new HashMap<node,TreeSet<node>>();
         int i = 0;
         for(node a : graph.getNodes()){
             TreeSet<node> dis = new TreeSet();
             dis.add(a);
             disjointSet.put(a,dis);
             for(node b : graph.getAdjSet(a)){
                 try{
                     EdgeClass ec = new EdgeClass(a,b,graph.getEdgeCost(a,b));
                     pq.add(ec);
                 }catch(Exception e){}
             }
         }

         Graph<node,cost> mst = new Graph<node,cost>();
         mst.setDirected(graph.isDirected());
         while(pq.size() > 0){
             EdgeClass ec = pq.poll();
             if(disjointSet.get(ec.from).first() != disjointSet.get(ec.to).first()){

                 TreeSet<node> set = disjointSet.get(ec.from);
                 set.addAll(disjointSet.get(ec.to));
                 for(node n : set){
                     disjointSet.put(n,set);
                 }
                 mst.addEdge(ec.from,ec.to,ec.weight);
             }
         }
         return mst;
     }

     /**
      * Topological sort - Sorting the graph such that all directed edges are going from left to right
      * This woule be impossible if the graph is cyclic. Exception will be thrown in such situations.
      * @param graph Graph object on wich nodes are ordered in topological order
      * @return ArrayList containing a list of nodes sorted in the topological order
      * @exception Exception If the graph contains cycles this method will throw an Exception
      */
      public ArrayList<node> topologicalSort(Graph<node,cost> graph) throws Exception{
          Set<node> nodes = graph.getNodes();
          LinkedList<node> myList = new LinkedList<node>(nodes);  //mySet contains the node that has no incomming edges
          // After the below loop myList will have nodes without no incomming edges
          for( node n : nodes){
                try{
                    Set<node> mnodes = graph.getAdjSet(n);
                    for( node m : mnodes){
                        myList.remove(m);
                    }
                }catch(Exception e){
                    System.err.println("Something went wront! ");
                }
          }
          //return is the path ArrayList
          ArrayList<node> path = new ArrayList<node>();

          Graph<node,cost> backup = new Graph<node,cost>();
          backup.setDirected(true);

          while(! myList.isEmpty() ){
                node n = myList.poll();
                path.add(n);
                Set<node> adj = new HashSet<node>( graph.getAdjSet(n) );    // since we are modifying the original graph we should get a clone of the original set.
                for(node m : adj){
                    cost c = graph.removeEdge(n,m);
                    backup.addEdge(n,m,c);
                    if( graph.getIncommingNodes(m).isEmpty() ){
                        myList.add(m);
                    }
                }
          }
          //if there are edges left on the graph there are cycles in the graph. Therefore cant be topologically sorted.
          Boolean isAcyclic = true;
          for(node n : nodes){
              if(! graph.getAdjSet(n).isEmpty() ) { isAcyclic = false;      break;  }
          }
          // Restore the backup edges
          Set<node> bn = backup.getNodes();
          for(node n : bn){
              Set<node> bm = backup.getAdjSet(n);
              for(node m : bm){
                  try{
                     graph.addEdge(n,m,backup.getEdgeCost(n,m) );
                  }catch(Exception e){
                      // should not come here
                  }
              }
          }

          if( !isAcyclic ) throw new Exception("Graph contains cycles...");
          return path;
      }

    /**
     * Ford-Folkson method
     * this will change the original graph
     */
    public Graph<node,cost> fordFolkson(Graph<node,cost> graph,node source,node sink){
        //extend the Graph object to a Flow network
        class FlowGraph<node,cost> extends Graph<node,cost>{
            HashMap<node,HashMap<node,cost>> flow;

            FlowGraph(Graph<node,cost> graph){
                super();
                flow = new HashMap<node,HashMap<node,cost>>();
                for(node n : graph.getNodes()){
                    this.graph.put(n,new HashMap<node,cost>());
                    this.flow.put(n,new HashMap<node,cost>());

                    HashMap<node,cost> flowAdjMap = this.flow.get(n);

                    for(node m : graph.getAdjSet(n)){
                        flowAdjMap.put(m,(cost) new Integer(0));
                    }
                }
            }

            private void removeAntiParallelEdges(){
                Set<node> nodes = this.getNodes();
                for(node n : nodes){
                    Set<node> nAdj = this.getAdjSet(n);
                    for(node m : nodes){
                        cost c = this.removeEdge(m,n);
                        if(c != null){  // if c == null there is no anti-parallel edge
                            node nm = (node) new Object();
                            this.addEdge(m,nm,c);
                            this.addEdge(nm,n,c);
                        }
                    }
                }
            }

            public cost getFlow(node from,node to) throws Exception{
                cost f = this.flow.get(from).get(to);
                if( f == null)
                    throw new Exception("Edge not found!");
                return f;
            }

            public void setFlow(node from,node to,cost f){
                Map<node,cost> outFlow = this.flow.get(from);
                if(outFlow != null){
                    outFlow.put(to,f);
                }
            }
        }

        FlowGraph flowgraph = new FlowGraph(graph);
        flowgraph.removeAntiParallelEdges();



        return null;
    }
}
