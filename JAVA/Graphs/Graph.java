import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class Graph<node,cost> {

    HashMap<node,HashMap<node,cost> > graph;
    Boolean directed;
    /**
	* Creates a new directed Graph Object<br>
	* Use setDrirected(false) method to make the graph undirected
	*/
    Graph(){
        graph = new HashMap<node,HashMap<node,cost> >();
        directed = true;
    }

    /**
     * Specify the Graph is a directed one or a not
     * @param isDirected true for directed graphs
     */
    public void setDirected(Boolean isDirected){
        this.directed = isDirected;
    }
    /**
	* Check if the graph is directed or not
	* @return true if the graph is directed
	*/
	public boolean isDirected(){
		return this.directed;
	}
    /**
     * Add a new node to the graph
     * @param a the node to be added
     *
     */
    public void addNode(node a){
        graph.put(a,new HashMap<node,cost>() );
    }

    /**
     * Add a new edge to the graph
     * if the nodes connected by the edge are not in the graph they will be added to the graph
     * on directed graphs edge will be from a to b
     * @param a edge will be leaving this node
     * @param b edge will be entering this node
     * @param c the cost of the edge
     */
    public void addEdge(node a,node b, cost c){

        if(! graph.containsKey(a))  addNode(a);

        if(! graph.containsKey(b))  addNode(b);
        //insert the new edge cost
        graph.get(a).put(b,c);

        if(! directed)  graph.get(b).put(a,c);
    }

    /**
     * @return A set of nodes of the graph
     */
    public Set<node> getNodes(){
        return graph.keySet();
    }
    /**
     * @param a the node on which adjacency list is found
     * @return A set of nodes adjacent to the given node
     */
    public Set<node> getAdjSet(node a){
        if(! graph.containsKey(a) ){
            return new HashSet<node>();
        }
        return graph.get(a).keySet();
    }
    /**
     * @param from Node that the edge is leaving
     * @param to Node that the edge is entering
     * @return cost The weight of the edge
     * @throws Exception will be thrown if the edge is not present
     */
    public cost getEdgeCost(node from,node to) throws Exception{
        cost c = graph.get(from).get(to);
        if( c == null)
            throw new Exception("Edge not found!");
        return c;
    }
    /**
     * @param from Node that the edge is leaving
     * @param to Node that the edge is enterring
     * @return cost of the removed edge. null if the edge not present
     */
    public cost removeEdge(node from,node to){
        cost c = null;
        if(graph.containsKey(from)){
            c = graph.get(from).remove(to);  // remove method of the HashMap returns null if the key(to) is not present
        }
        if(!this.directed){
            c = graph.get(to).remove(from);
        }
        return c;
    }
    /**
     * This method is used in topological sort algorithm
     * @param child Node with the head of a edge in the graph
     * @return Set of nodes with the tail of a edge in the graph
     */
    public Set<node> getIncommingNodes(node child){
        Set<node> result = new HashSet<node>();
        Set<node> nodes = this.getNodes();
        for(node n : nodes){
            if(this.getAdjSet(n).contains(child)){
                result.add(n);
            }
        }
        return result;
    }
}
