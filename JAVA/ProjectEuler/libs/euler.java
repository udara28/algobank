import java.util.*;
class euler{
	
//Alphebatical sort in exchange method
	public static void alphebaticSort(String x[]){
		String temp;
		for(int i=0;i<x.length-1;i++){
			for(int j=i+1;j<x.length;j++){
				if(x[i].compareToIgnoreCase(x[j])>0){
					temp=x[i];
					x[i]=x[j];
					x[j]=temp;
				}
			}
		}
	}
	
//Alphebatical sort in bubble sort method
	public static void alphebaticBubbleSort(String x[]){
		String temp;
		boolean flag=true;
		while(flag){
			flag=false;
			for(int i=0;i<x.length-1;i++){
				if(x[i].compareToIgnoreCase(x[i+1])>0){
					temp=x[i];
					x[i]=x[i+1];
					x[i+1]=temp;
					flag=true;
				}
			}
		}
	}
	
//Find the sum of proper factors of number n
	public static int sumOfFactors(int x){
		int sum=0;
		int j=(int)	Math.sqrt(x)+1;
		for(int i=2;i<j;i++){
			if(x%i==0){
				if((i*i)!=x)	sum+=(i+(x/i));
				else	sum+=i;	
			}
		}
		sum++;
		return sum;
	}
	
//Find the factorial of number n
	public static int factorial(int n){
		int k=1;
		if(n<0)	return -1;
		else if(n==0)	return 1;
		else{
			for(int i=1;i<=n;i++){
				k*=i;
			}	
		}
		return k;
	}
	
//check if n is a prime number
	public static boolean isPrime(int n){
		int i=(int) Math.sqrt(n)+1;
		if(n<2)	return false;
		for(int j=2;j<i;j++){
			if(n%j==0)	return false;
		}
		return true;
	}
	
//Pandigital numbers where 0 is not considered!!
	public	static boolean isPandigital(int i){
    	int di[]=new int[10];
    	di[0]=1;
    	int j=i,x=0;
    	while(j!=0){
    		x++;
    		j=j/10;
    	}
    	for(int y=9;y>x;y--)	di[y]=1;
    	while(i!=0){
    		if(di[i%10]!=0)	return false;
    		else	di[i%10]=1;
    		i=i/10;
    	}
    	return true;
    }
    
//Seperate digits of a number and returns a list
	public static ArrayList<Integer> digitSeperator(int i){
		ArrayList<Integer> digits= new ArrayList<Integer>();
		while(i!=0){
			digits.add(i%10);
			i=i/10;
		}
		return digits;		
	}
//Combine digits of a number and return the long
	public static long digitCombiner(ArrayList<Integer> di){
		long num=0;
		for(int i=0;i<di.size();i++){
			num+=(long) (di.get(i)*Math.pow(10,i));
		}
		return num;
	}
}