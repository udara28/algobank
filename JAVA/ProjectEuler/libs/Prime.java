class Prime{
	static int nextNumber(int i){
		int j=0;
		int out=(i+1);
		while(j==0){
			for(int x=(out/2); x>1;x--){
				int c=out%x;
				if(c==0){
					j=2;
					break;
				}
			}
			if(j==2){
				out++;
				j=0;
			}else{
				j=1;
			}
		}
		return out;
	}
}
