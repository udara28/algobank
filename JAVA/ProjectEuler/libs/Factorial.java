class Jmaths{
	static int findFactorial(int i){
		int k=1;
		while(i>0){
			k=k*i;
			i--;
		}
		return k;
	}
	
	static int findnCr(int n,int r){
		int c=Jmaths.findFactorial(n)/(Jmaths.findFactorial(n-r)*Jmaths.findFactorial(r));
		return c;
	}
}