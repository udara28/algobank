class Factors{
	static long[] allFactors(long i){
		int k=1;
		for(long j=i/2;j>0;j--){
			long c=i%j;
			if(c==0){
				k++;
			}
		}
		long []fac= new long[k];
		fac[k-1]=(int)i;
		for(long j=i/2;j>0;j--){
			long c=i%j;
			if(c==0){
				k--;
				fac[k-1]=j;
			}
		}
		return fac;
	}
}