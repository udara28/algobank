class euler_3{
	public static void main(String args[]){
		long num=600851475143L;
		int devider=2;
		while(num!=1){
			if(num%devider!=0){
				devider=Prime.nextNumber(devider);
			}else{
				num=num/devider;
			}
		}
		System.out.println("the biggest prime number is "+devider);	
	}
}
