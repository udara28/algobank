import java.io.*;
import java.util.*;

public class euler_105 {
    public static void main(String args[]) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(args[0]));
        String line;
		long total = 0;
        while((line = br.readLine() ) != null ) {
            total += (long)sumSpecialSubSet(line);
        }
		System.out.println(total);
    }

    static long sumSpecialSubSet(String line) {
        String[] args = line.split(",");
        ArrayList<Integer> arr = new ArrayList<Integer>();
        for(String s : args) {
            arr.add(Integer.valueOf(s));
        }
		if( ! hasEqualSumSubSets(arr) && condition2Holds(arr) ){
			int sum = 0;
			for( int i : arr )	sum += i;
			return sum;
		}
        return 0;
    }

	static boolean hasEqualSumSubSets(List<Integer> list){
		boolean hasEquals = false;
		
        SetAlgorithms<Integer> setAlgo = new SetAlgorithms<Integer>();	// this class is implemented in AlgoBank/JAVA/Math/SetAlgorithms.java
        List<Set<Integer>> setList = setAlgo.allSubSets(list);

		Map<Integer,List<Integer>> sumMap = new HashMap<Integer,List<Integer>>();
		for(Set<Integer> set : setList){
			int sum = 0;
			for(int i : set){
				sum += i;
			}

			if( sumMap.containsKey(set.size()) ){
						List<Integer> sumList = sumMap.get(set.size());
						for(int i : sumList)
							if(i == sum)	return true;
						sumList.add(sum);
			}else{
				List<Integer> sumList = new ArrayList<Integer>();
				sumList.add(sum);
				sumMap.put(set.size(), sumList);
			}
			
			if(hasEquals) return hasEquals;
		}
		return hasEquals;
	}

	static boolean condition2Holds(List<Integer> list){
		Collections.sort(list);
		// { A, B, C, D, E} --> A + B + C < D + E => condition2 is false
		int stop = list.size()/2 + list.size()%2;
		int pivot = 1;	// no need for test for 0
		boolean condition = true;
		while(pivot != stop && condition){
			int minSum = 0;
			for(int i=0; i <= pivot ; i++){
				minSum += list.get(i);
			}
			int maxSum = 0;
			for(int i=list.size()-1; i >= list.size()-pivot; i--){
				maxSum += list.get(i);
			}
			if( !(minSum > maxSum) )	condition = false;
			pivot++;
		}
		return condition;
	}
}
