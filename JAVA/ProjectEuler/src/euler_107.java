import java.util.*;
import java.io.*;

class euler_107{
	public static void main(String args[]) throws Exception{
		BufferedReader br = new BufferedReader(new FileReader(args[0]));
		String s;
		int lineCount = 0;
		Graph<Integer,Long> graph = new Graph<Integer,Long>();
		graph.setDirected(false);
		while((s = br.readLine()) != null){
			String a[] = s.split(",");
			for(int i=0; i<a.length; i++){
				if(a[i].contains("-")){
					continue;
				}
				long cost = Long.parseLong(a[i]);
				graph.addEdge(lineCount,i,cost);
			}
			lineCount++;
		}
		br.close();	

		long initalCost = totalEdgeCost(graph,false)/2;
		System.out.println("Initial : "+initalCost);

		GraphAlgorithms<Integer,Long> algo = new GraphAlgorithms<Integer,Long>();
		Graph<Integer,Long> miniGraph = algo.primsMST(graph);

		long miniCost = totalEdgeCost(miniGraph,false)/2;
		System.out.println("After : "+miniCost);

		System.out.println("Saving : "+(initalCost-miniCost));
	}

	static long totalEdgeCost(Graph<Integer,Long> graph,boolean print){
		Set<Integer> nodes = graph.getNodes();
		long total = 0;
		int nodeCount = 0;
		int edgeCount = 0;
		for(Integer a : nodes){
			nodeCount++;
			Set<Integer> adjNodes = graph.getAdjSet(a);
			for(Integer b : adjNodes){
				edgeCount++;
				try{
					total += graph.getEdgeCost(a,b);
					if(print && a > b) System.out.println( a + "->"+ b+" : "+graph.getEdgeCost(a,b));
				}catch(Exception e){
					System.out.println("Error: "+e.getMessage());
				}
			}
		}
		return total;	//since undirected graph total is double the cost
	}
}
