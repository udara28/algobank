import java.util.*;

class euler_106{

	public static void main(String args[]){
		int count = 0;
		int key = Integer.parseInt(args[0]);
		Combinatorics com = new Combinatorics(key);
		for(int i=4; i<=key; i+=2){
			count += com.nCr(key,i)*countOfSetNeedsAttention(i);
		}
		System.out.println("Answer : "+count);
	}

	static int countOfSetNeedsAttention(int pivot){
		boolean t[] = new boolean[pivot];
		int counter = 0;
		int com[][] = Combinatorics.selectRFromN(pivot,pivot/2);
		for(int i=0; i<com.length; i++){
			t = new boolean[pivot];
			for(int j : com[i])
				t[j] = true;
			Queue<Integer> set1 = new LinkedList<Integer>();
			Queue<Integer> set2 = new LinkedList<Integer>();
			for(int j=0; j<t.length; j++)
				if(t[j])
					set1.add(j);
				else
					set2.add(j);
			boolean s1,s2;
			s1 = s2 = false;
			while(!set1.isEmpty()){
				if(set1.poll() > set2.poll())
					s1 = true;
				else
					s2 = true;
				if(s1 & s2){
					counter++;
					break;
				}
			}			
		}
		return (counter/2);
	}
}
