import java.util.*;
import java.math.*;

class euler_104{

	static BigInteger TEN = BigInteger.valueOf(10);

	public static void main(String args[]){
		BigInteger f1 = BigInteger.ONE;
		BigInteger f2 = BigInteger.ONE;
		int exp = 1;
		BigInteger tmp;
		int count = 2;
		do{
			do{
				tmp = f2.add(f1);
				count++;
				f1 = f2;
				f2 = tmp;
				if(f2.max(TEN.pow(exp)).equals(f2)) exp++;
			}while(!lastTenIsPandigital(f2));
			System.out.println(count);
		}while(!firstTenIsPandigital(f2,exp));

		System.out.println("Answer : "+count+" : "+f2.divide(TEN.pow(exp-9)).toString());
	}

	static boolean firstTenIsPandigital(BigInteger b,int exp){
		if(exp < 9)
			return false;
		
		return isPandigital(b.divide(TEN.pow(exp-9)).longValue());
	}

	static boolean lastTenIsPandigital(BigInteger b){
		return isPandigital(b.mod(TEN.pow(9)).longValue());
	}

	static boolean isPandigital(long l){
		boolean b[] = new boolean[10];
		while(l != 0){
			b[(int)l%10] = true;
			l = l / 10;
		}
		for(int i=1; i<10; i++)
			if(!b[i]) return false;

		return true;
	}
}
