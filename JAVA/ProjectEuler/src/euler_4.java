class euler_4{
	public static void main(String args[]){
		int ans=0;
		L1: for(int i=999;i>0;i--){
			L2: for(int j=999;j>100;j--){
				ans=i*j;
				//assume that the digits of of ans as abcdef
				int a=separate(ans,100000);
				int b=separate(ans,10000);
				int c=separate(ans,1000);
				int d=separate(ans,100);
				int e=separate(ans,10);
				int f=separate(ans,1);
				if(b==e && a==f && c==d && a>8){
					System.out.println(ans);
					System.out.println(i);	
					System.out.println(j);				
				}
			}
		}
		System.out.println("the largest palindromic number is "+ans);
	}

	static int separate(int i,int j){
		int a=i%(j*10);
		int b=a/j;
		return b;
	}
}
