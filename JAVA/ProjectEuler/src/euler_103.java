import java.util.*;

class euler_103{
	public static void main(String args[]){
		/*	we found a special sum set where sum is 255
		 	20 31 38 39 40 42 45
			we need to check whether there are sets that perform better 
		*/
		int num[] = {20, 31, 38, 39, 40, 42, 44};
		int e = 5; // this is the error we are allowing.
		// bigger e value give better results but takes more time 
		for(int t1 = -1*e; t1 <= e; t1++){
		  for(int t2 = -1*e; t2 <= e; t2++){
		    for(int t3 = -1*e; t3 <= e; t3++){
			  for(int t4 = -1*e; t4 <= e; t4++){
				for(int t5 = -1*e; t5 <= e; t5++){
				  for(int t6 = -1*e; t6 <= e; t6++){
					for(int t7 = -1*e; t7 <= e; t7++){
						
						List<Integer> list = new ArrayList<Integer>();
						list.add(num[0] - t1);
						if(num[1]-t2 > num[0]-t1) list.add(num[1] - t2); else break;
						if(num[2]-t3 > num[1]-t2) list.add(num[2] - t3); else break;
						if(num[3]-t4 > num[2]-t3) list.add(num[3] - t4); else break;
						if(num[4]-t5 > num[3]-t4) list.add(num[4] - t5); else break;
						if(num[5]-t6 > num[4]-t5) list.add(num[5] - t6); else break;
						if(num[6]-t7 > num[5]-t6) list.add(num[6] - t7); else break;

						if(!euler_105.hasEqualSumSubSets(list) &&
								euler_105.condition2Holds(list)    ){
							int sum = 0;
							for(int i : list){
								System.out.print(i+" ");
								sum += i;
							}
							System.out.println(" :\tsum = "+sum);
						}
	
					}
				  }
				}
			  }
			}
		  }
		}
	}
}
