import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;

public class Primes{

	List<Integer> primeList;	

	public Primes(int limit){
		primeList = simplePrimes(limit);
	}
	/**
	*Returns a list of prime factors of the given number
	*to use this method a Primes object should be created with a good enough upper limit
	*Ex: 12 = [ 2, 3 ]
	*@param number the number of which prime factors were calculated
	*@return List of the prime factors
	*/
	public List<Integer> primeFactors(int number){
		List<Integer> factors= new ArrayList<Integer>();
		for(int i=0; i < primeList.size(); i++){
			if(number % primeList.get(i) == 0)	factors.add(primeList.get(i));
		}
		return factors;
	}

	/**
	*Simple but fast algorithm to calculate primes up to some limit
	*@param limit the limit upto which primes were calculated
	*@return List of primes
	**/
	public static List<Integer> simplePrimes(int limit){
		boolean not_prime[] = new boolean[limit+1];
		not_prime[0] = not_prime[1] = true;
		int nextPrime = 2;
		boolean done = false;
		while(!done){
			for(int j = 2*nextPrime; j <= limit; j += nextPrime)
				not_prime[j] = true; 
			
			do{
				nextPrime++;
				if(nextPrime > limit) done = true;
			}while(!done && not_prime[nextPrime]);
		}
		List<Integer> primes = new ArrayList<Integer>();
		for(int i = 0; i < not_prime.length; i++)
			if(!not_prime[i]) primes.add(i);

		return primes;
	}

	/**
	*Fast algorithm to generate primes<br>
	*but it seems simplePrimes perform okay when compared with the code complexity<br>
	*Use this to generate very large primes ( Largest seen in a short time is 199999991 
	*Recommend using simplePrimes instead of sieveOfAtkin
	*Pseudo code link : <a href="http://en.wikipedia.org/wiki/Sieve_of_Atkin">http://en.wikipedia.org/wiki/Sieve_of_Atkin</a>
	*@param limit the limit up to which primes were calculated
	*@return List of primes
	**/	
	public static List<Long> sieveOfAtkin(long limit){
		//let's work assuming the number format is long
		int s[] = {1, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 49, 53, 59};
		
		class SieveList {
			private boolean primeList[][];
			long maxI = Integer.MAX_VALUE;
			SieveList(long limit){
				primeList = new boolean[(int)(limit / maxI)+1][(int)(limit % maxI)+1];
				for(boolean b[] : primeList)
					Arrays.fill(b,true);
			}
			public void set(long index,boolean value){
				primeList[(int)(index / maxI)][(int)(index % maxI)] = value;
			}
			public boolean get(long index){
				return primeList[(int)(index / maxI)][(int)(index % maxI)];
			}
		}
		//boolean is_prime[] = new boolean[limit+1]; // sieve list
		SieveList sl = new SieveList(limit+1);

		//Arrays.fill(is_prime,true);

		for( int x : s ){
			int w = 0;
			while( 60*w + x <= limit){
			//	is_prime[ 60*w + x ] = false;
				sl.set(60*w + x, false);
				w++;
			}
		}

		long x,y,n;
		boolean done;

		x = 1; y = 1; done = false;
		int a1[] = { 1, 13, 17, 29, 37, 41, 49, 53};
		while(!done){
			y = 1;
			long XX4 = 4*x*x;
			n = XX4 + y*y;
			done = true;
			while(n <= limit){
				done = false;

				if(Arrays.binarySearch(a1, (int) n%60) >= 0){
				//	is_prime[n] = !is_prime[n];
					sl.set(n, ! sl.get(n));
				}
				y += 2;	//odd y's
				n = XX4 + y*y;
			}
			x++;
		}
		x = 1; y = 2; done = false;
		int a2[] = { 7, 19, 31, 43};
		while(!done){
			y = 2;
			long XX3 = 3*x*x;
			n = XX3 + y*y;
			done = true;
			while(n <= limit){
				done = false;

				if(Arrays.binarySearch(a2, (int) n%60) >= 0){
				//	is_prime[n] = !is_prime[n];
					sl.set(n, ! sl.get(n));				
				}
				y += 2;
				n = XX3 + y*y;
			}			
			x += 2;
		}
		x = 2; y = 1; done = false;
		int a3[] = { 11, 23, 47, 59};
		while(!done){
			y = x-1;
			long XX3 = 3*x*x;
			n = XX3 - y*y;
			done = true;
			while(n <= limit && y > 0){
				done = false;

				if(Arrays.binarySearch(a3, (int) n%60) >= 0){
				//	is_prime[n] = !is_prime[n];
					sl.set(n, ! sl.get(n));
				}
				y -= 2;
				n = XX3 - y*y;
			}
			x++;
		}

		long w = 0; long c; done = false; 
		while(!done){
			y = 1;
			x = s[(int)y];	// x >= 7
			n = 60*w + x;
			done = true;
			while(n*n <= limit){
				done = false;					

				if(sl.get(n)){
					long m,k = 0;
					m = s[(int)k];
					c = n*n*m;
					while( c <= limit ){
					//	is_prime[c] = false;
						sl.set(c, false);
						k++;
						m = (k < s.length)? s[(int)k] : k;
						c = n*n*m;
					}
				}			
	
				y++;
				if(y == s.length) break;
				x = s[(int)y];
				n = 60*w + x;
			}
			w++;
		}
		List<Long> primes = new ArrayList<Long>();
		primes.add((long)2);	primes.add((long)3);	primes.add((long)5);
		
		for(int x1 : s ){
			w = 0;
			n = 60*w + x1;
			while(n <= limit){
				if(sl.get(n)) primes.add(n);
				w++;
				n = 60*w + x1 ;
			}
		}

		return primes;
	}
}
