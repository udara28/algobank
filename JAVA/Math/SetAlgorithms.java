import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

public class SetAlgorithms<T>{
	
	public List<Set<T>> allSubSets(Set<T> origin){
		return allSubSets(new ArrayList<T>(origin));
	}

	public List<Set<T>> allSubSets(List<T> origin){
		List<Set<T>> all = new ArrayList<Set<T>>();	
		//below code is a binary counter
		byte[] counter = new byte[origin.size()];
		//each binary number represent a subset
		while(true){
			//use the counter value to create a subset
			Set<T> subset = new HashSet<T>();
			for(int i = 0; i < counter.length; i++){
				if(counter[i] != 0){
					subset.add(origin.get(i));
				}
			}
			all.add(subset);
			//increment the counter
			int i = 0;
			while( i < counter.length && counter[i] == 1)
				counter[i++] = 0;
			if(i == counter.length)
				break;
			counter[i] = 1;
		}		
		return all;
	}
}
