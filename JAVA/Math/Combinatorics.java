import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

public class Combinatorics{
	
	private Primes primes;
	private int maxLimit;	
	/**
	*Constructor method
	*@param maxLimit upper bound of the all calculations done using the object
	*/
	public Combinatorics(int maxLimit){
		primes = new Primes(maxLimit);
		this.maxLimit = maxLimit;
	}
	/**
	*This method give the value of n choose r<br>
	*If the value exceeds Long.MAX_VALUE consider using ncrMap instead<br>
	*@see ncrMap
	*@param n N number
	*@param r R number
	*@return value of nCr
	*/
	public long nCr(int n,int r){
		return mapIntegerValue(ncrMap(n,r));
	}
	/**
	*Simple algorithm to calculate ncr using dynamic programming<br>
	*<b>If value is large use nCr instead</b>
	*@see nCr
	*@param n N number
	*@param r R number
	*@return value of ncr
	*/
	public static long ncr(int n,int r){
		long row[] = new long[n+1];
		row[0] = 1;
		for(int i = 1; i <= n ; i++)
			for(int j = i; j > 0 ; j--)
				row[j] += row[j-1];		// from the recurrence C(n, r) = C(n - 1, r - 1) + C(n - 1, r)
		return row[r];
	}
	/**
	*This will give the value of n choose r<br>
	*make sure that n does not exceed the maxLimit of the object<br>
	*to get the value use mapIntegerValue() method
	*@see mapIntegerValue
	*@param n N number
	*@param r R number
	*@return Map of the value of nCr
	*/
	public Map<Integer,Integer> ncrMap(int n, int r){
		if(n > maxLimit)
			System.err.println("Error: n exceeds the max-limit of this object");
		Map<Integer,Integer> nfacs = factorialMap(n);
		Map<Integer,Integer> rfacs = factorialMap(r);
		Map<Integer,Integer> nrfacs= factorialMap(n-r);
		
		for(Map.Entry<Integer,Integer> entry : rfacs.entrySet()){
			int tmp = nfacs.get(entry.getKey()) - entry.getValue();
			nfacs.put(entry.getKey(), tmp );
		}
		for(Map.Entry<Integer,Integer> entry : nrfacs.entrySet()){
			int tmp = nfacs.get(entry.getKey()) - entry.getValue();
			nfacs.put(entry.getKey(), tmp );
		}
		
		return nfacs;
	}
	/**
	*This will give the permutation of n choose r<br>
	*If the value exceeds Long.MAX_VALUE consider using nprMap() instead
	*@see nprMap
	*@param n N number
	*@param r R number
	*@return value of nPr
	*/
	public long nPr(int n, int r){
		return mapIntegerValue(nprMap(n,r));
	}
	/**
	*This will give the value of nPr<br>
	*make sure that n does not exceed the maxLimit of the object<br>
	*to get the value from the map use mapIntegerValue() method
	*@see mapIntegerValue
	*@param n N number
	*@param r R number
	*@return Map of the value of nPr
	*/
	public Map<Integer,Integer> nprMap(int n, int r){
		if(n > maxLimit)
			System.err.println("Error: n exceeds the max-limit of this object");
		Map<Integer,Integer> nfacs = factorialMap(n);
		Map<Integer,Integer> nrfacs= factorialMap(n-r);

		for(Map.Entry<Integer,Integer> entry : nrfacs.entrySet()){
			int tmp = nfacs.get(entry.getKey()) - entry.getValue();
			nfacs.put(entry.getKey(), tmp );
		}		
		return nfacs;
	}
	
	/**
	*This will calculate the factorial of the given number<br>
	*<b>DO NOT USE IF n IS GREATER THAN 20</b><br>
	*if n is greater than 20 consider using factorialMap instead
	*@see factorialMap
	*@param n number of which factorial is calculated
	*@return value of the factorial 
	*/
	public long factorial(int n){
		Map<Integer,Integer> map = factorialMap(n);
		long out = 1;
		for(Map.Entry<Integer,Integer> entry : map.entrySet()){
			Double d = new Double(Math.pow(entry.getKey(), entry.getValue()));
			out *= d.longValue();
		}
		return out;
	}
	/**
	*This function can be used to for calculating factorial of very large numbers<br>
	*instead of returning the factorial this returns a map containing it's prime factors
	*@param n number of which factorial is calculated
	*@return map containing all prime factors as key and there respective powers as values
	*/
	public Map<Integer,Integer> factorialMap(int n){
		Map<Integer,Integer> out = new HashMap<Integer,Integer>();
		for(int i = 2; i <= n; i++){
			Map<Integer,Integer> map = factorize(i,primes.primeFactors(i));
			for(Map.Entry<Integer,Integer> entry : map.entrySet() ){
				if(out.containsKey(entry.getKey())){
					int tmp = out.get(entry.getKey()) + entry.getValue();
					out.put(entry.getKey(), tmp); 
				}else{
					out.put(entry.getKey(), entry.getValue());
				}
			}
		}
		return out;
	}
	/**
	*This gives a factorized map view of the number
	*@param n number of which factors were found
	*@param facs list of prime factors. This can be found using primeFactors method in Primes
	*@return map containing all prime factors as keys and there respective powers as values 
	*/
	public static Map<Integer,Integer> factorize(int n,List<Integer> facs){
		Map<Integer,Integer> map = new HashMap<Integer,Integer>();
		for(int f : facs){
			int count = 0;
			int tmp = n;
			while(tmp % f == 0){
				count++;
				tmp = tmp/f;
			}
			map.put(f,count);
		}
		return map;
	}
	/**
	*It is common to take the modulo of a nCr value since they are very large<br>
	*This method is used for this purpose<br> Ex : nCr % 1000000007
	*@param mapInteger map containing prime factors as keys and there powers as values
	*@param modulo Modulo value usually a prime like 1000000007
	*@return the value of mapInteger % modulo
	*/
	public static long mapIntegerValue(Map<Integer,Integer> mapInteger, long modulo){
		long out = 1;
		for(Map.Entry<Integer,Integer> entry : mapInteger.entrySet()){
			for(int i = 0; i < entry.getValue(); i++)
				out = (out * entry.getKey()) % modulo;
		}
		return out;
	}
	/**
	*This method convert a mapInteger to its value<br>
	*It is common to take the modulo of a prime mapIntegerValue(map,mod)
	*@param mapInteger map containing prime factors as keys and there powers as values
	*@return long value of the map
	*/
	public static long mapIntegerValue(Map<Integer,Integer> mapInteger){
		long out = 1;
		for(Map.Entry<Integer,Integer> entry : mapInteger.entrySet()){
			for(int i = 0; i < entry.getValue(); i++)
				out = out * entry.getKey();
		}
		return out;
	}
	/**
	*This method returns different possible sets of size r that can be choosen from a set of size n<br>
	*Ex : ways of selecting 2 from 3
	*<ul> <li>(0,1)</li> <li>(0,2)</li> <li>(1,2)</li> </ul>
	*@param n N number
	*@param r R number
	*@return 2d array of size [nCr][r]
	*/
	public static int[][] selectRFromN(int n, int r){
		int result[][] = new int[(int)ncr(n,r)][r];
		int result_counter = 0;

		int sel[] = new int[r];
		for(int i=0; i<r ; i++)	sel[i] = i;
		int p = r-1;
		while(p >= 0){
			p = r-1;
			while(sel[p] <= (n+p)-r){
				for(int i=0; i<r; i++)
					result[result_counter][i] = sel[i];
				result_counter++;
			//	System.out.println(Arrays.toString(sel));
				sel[p]++;
			}
			sel[p]--;
			while(p >= 0 && sel[p] == (n+p)-r){
				p--;
			}
			if(p >= 0){
				sel[p]++;
				for(int i=p+1;i<r;i++){
					sel[i] = sel[i-1]+1;
				}
			}
		}
		return result;
	}
}
