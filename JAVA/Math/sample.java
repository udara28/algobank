import java.util.*;
class sample{
	public static void main(String args[]){
		long l = Long.parseLong(args[0]);
		List<Long> list = Primes.sieveOfAtkin(l);
		Collections.sort(list);
		System.out.println(list.get(list.size()-1));
		/*
		int p = Integer.parseInt(args[0]);
		Combinatorics com = new Combinatorics(p);
		System.out.println(p+"! = "+com.factorial(p));
			
		for(int i = p-4; i <= p; i++){
			System.out.println(i+" => "+com.mapIntegerValue(com.nprMap(p,i),(long) 1000000007));
		}
		*/
		/*
		//List<Integer> list = Primes.sieveOfAtkin(1000000);
		//List<Integer> list = Primes.simplePrimes(1000000);
		List<Integer> list = Primes.primeFactors(510);
		for(int p : list){
			System.out.print(p+"  ");
		}
		System.out.println();
		*/
	}
}
