from time import time
from random import randrange
from Sorting import *

li = []
for i in range(1000):
    li.append(randrange(1000))
#li = [34,65,2,56,89,234,64,65,46,123,2]

# timing of quick sort
qli = list(li)
start_time = time()
quickSort(qli,0,len(qli)-1)
end_time = time()
print("Quick Sort Time : {0:0.5f}".format(end_time-start_time))

# timing of merge sort
mli = list(li)
start_time = time()
mergeSort(mli,0,len(mli)-1)
end_time = time()
print("Merge Sort Time : {0:0.5f}".format(end_time-start_time))
#print li
    