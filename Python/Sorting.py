# input : unsorted subarray A[p..r]
# output: sorted subarray A[p..r]
    
def quickSort(A,p,r):
    if(p<r):
        q = _partition(A,p,r)
        quickSort(A,p,q-1)
        quickSort(A,q+1,r)
        
def _partition(A,p,r):
    pivot = A[r]
    i = p
    for j in range(p,r): # range [p:r] inclusive
        if A[j] < pivot :
            tmp  = A[i]
            A[i] = A[j]
            A[j] = tmp
            i = i+1
    tmp  = A[i]
    A[i] = A[r]
    A[r] = tmp
    return i
    
def mergeSort(A,p,r):
    if(p<r):
        q = (p+r)/2
        mergeSort(A,p,q)
        mergeSort(A,q+1,r)
        A[p:r+1] = _merge(A,p,q,r)
    
        
def _merge(A,p,q,r):
    M = []
    i = p
    j = q+1
    while(i<=q):
        if(j<=r and A[j]<A[i]):
            M.append(A[j])
            j = j+1
        else:
            M.append(A[i])
            i = i+1
    while(j<=r):
        M.append(A[j])
        j = j+1
    return M