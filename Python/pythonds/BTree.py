class BRoot:
    def __init__(self):
        print "root created"

class BNode:
    _t = 4 # branching factor of BTree
    
    def __init__(self):
        self.n    = 0
        self.c    = [None for i in xrange(0,2*self._t)]
        self.key  = ['' for i in xrange(0,2*self._t-1)]
        self.leaf = True

def DiskWrite(x):
    s = '';
    for i in range(0,x.n) :
        s = s + str(x.key[i]) + '  '
    print "written to disk : "+s
    
def DiskRead(x):
    print "read from disk"

def BTreeCreate(T):
    x = BNode()
    x.leaf = True
    x.n    = 0
    DiskWrite(x)
    T.root = x
    
def BTreeSplitChild(x,i):
    z = BNode()     # z will have the second half of x.c[i] after split
    y = x.c[i]      # y will have the first half of x.c[i]
    z.leaf = y.leaf
    z.n = BNode._t - 1
    for j in range(0,BNode._t-1):
        z.key[j] = y.key[j+BNode._t]
    if not y.leaf:
        for j in range(0,BNode._t):
            z.c[j] = y.c[j+BNode._t]
    y.n = BNode._t - 1
    for j in range(x.n,i,-1):
        x.c[j+1] = x.c[j]
    x.c[i+1] = z
    for j in range(x.n-1,i-1,-1):
        x.key[j+1] = x.key[j]
    x.key[i] = y.key[BNode._t-1]
    x.n = x.n+1
    DiskWrite(y)
    DiskWrite(z)
    DiskWrite(x)
    
def BTreeInsert(T,k):
    r = T.root
    if r.n == 2*BNode._t - 1 :
        s = BNode()
        T.root = s
        s.leaf = False
        s.n    = 0
        s.c[0] = r
        BTreeSplitChild(s,0)
        BTreeInsertNonFull(s,k)
    else:
        BTreeInsertNonFull(r,k)

def BTreeInsertNonFull(x,k):
    i = x.n - 1
    if x.leaf :
        while i >= 0 and k < x.key[i] :
            x.key[i+1] = x.key[i]
            i = i - 1
        x.key[i+1] = k
        x.n = x.n + 1
        DiskWrite(x)
    else:
        while i >= 0 and k < x.key[i] :
            i = i - 1
        
        i = i+1
        DiskRead(x.c[i])
        if x.c[i].n == 2*BNode._t - 1 :
            BTreeSplitChild(x,i)
            if k > x.key[i] :
                i = i + 1
        BTreeInsertNonFull(x.c[i],k)

def BTreeSearch(x,k):
    i = 0
    while i < x.n and k > x.key[i] :
        i = i + 1
    if i < x.n and k == x.key[i] :
        return (x,i)
    elif x.leaf :
        return None
    else :
        DiskRead(x.c[i])
        return BTreeSearch(x.c[i],k)

T = BRoot()
BTreeCreate(T)
test_keys = ['V','C','E','T','X','I','B','P','Q','G','S','W','Z']
for key in test_keys :
    BTreeInsert(T,key)
print '-------------- SEARCH -----------------'
(x,i) = BTreeSearch(T.root,'S')
for i in range(x.n):
    print x.key[i]
        