################################ BST ###########################################
class Node:
    def __init__(key):
        self.key   = key
        self.left  = None
        self.right = None
        self.parent= None
    
def treeInsert(T,n):
    t  = T.root
    pT = None
    while(t != None):
        pT = t
        if(t.key > n.key):
            t = t.right
        else:
            t = t.left
    
    n.parent = pT
    if(pT == None):
        T.root = n
    elif(n.key > pT.key):
        pT.right = n
    else:
        pT.left  = n
        
def treeDelete(T,n):
    if(n.left == None):
        _transplant(T,n,n.right)
    elif(n.right == None):
        _transplant(T,n,n.left)
    else:
        ns = treeMin(T,n.right)
        if(ns.parent != n ):
            _transplant(T,ns,ns.right)
            ns.right = n.right
            ns.right.parent = ns
        _transplant(T,n,ns)
        ns.left = n.left
        ns.left.parent = ns
    
def _transplant(T,u,v):
    if(u.parent == None):
        T.root = v
    elif(u == u.parent.left):
        u.parent.left = v
    else:
        u.parent.right= v
        
    if(v != None):
        v.parent = u.parent
        
def treeSearch(T,key):
    if(T == None or T.key == key):
        return T
    if(key > T.key):
        return treeSearch(T.right,key)
    else:
        return treeSearch(T.left,key)

def treeMin(T):
    if(T == None or T.left == None):
        return T
    else:
        return treeMin(T.left)
        
def treeMax(T):
    if(T == None or T.right == None):
        return T
    else:
        return treeMax(T)
        
def treeSuccessor(T,n):
    if(n.right != None):
        return treeMin(n.right)
    else:
        while(n.parent != None and n.parent.right == n)
            n = n.parent
    
        
def leftRotate(T,n):
    tmp = n.right
    n.right    = tmp.left
    if(n.right != None):
        n.right.parent = n
    tmp.parent = n.parent
    if(n.parent == None):
        T.root = tmp
    elif(n == n.parent.right):
        n.parent.right = tmp
    else:
        n.parent.left  = tmp    
    
    tmp.left   = n
    tmp.left.parent = tmp
    
def rightRotate(T,n):
    tmp = n.left
    n.left = tmp.right
    if(n.left != None):
        n.left.parent = n
    tmp.parent = n.parent
    if(n.parent == None):
        T.root = tmp
    elif(n == n.parent.right):
        n.parent.right = tmp
    else:
        n.parent.left  = tmp
        
    tmp.right = n
    tmp.right.parent = tmp
        
########################## RED BLACK TREE ######################################

def rbInsert(T,n):
    treeInsert(n)
    n.color = 'red'
    while( n != T.root and n.parent.color == 'red' ):
        if( n.parent == n.parent.parent.left ):
            y = n.parent.parent.right # uncle/aunt of n
            if( y.color == 'red' ):         ### CASE 1 ###
                n.parent = 'black'    # set parent and uncle/aunt color to black
                y        = 'black'      
                n.parent.parent = 'red'
                n = n.parent.parent   # move n to grand parent of n to see is there any violations
                
            else if( n == n.parent.right):  ### CASE 2 ###
                leftRotate(T,n.paret) # convert to CASE 3
                
            else:                           ### CASE 3 ###
                n.parent.parent.color = 'red'
                n.parent.color        = 'black'
                rightRotate(T,n.parent.parent)
                
        else : # same thing with right left swapped
            y = n.parent.parent.left
            if(y.color == 'red'):
                n.parent = 'black'
                y        = 'black'
                n.parent.parent = 'red'
                n = n.parent.parent
                
            else if( n == n.parent.left):
                rightRotate(T,n.parent)
                
            else:
                n.parent.parent.color = 'red'
                n.parent.color        = 'black'
                leftRotate(T,n.parent.parent)

        
    
    